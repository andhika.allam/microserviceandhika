package com.emerio.bootcamp.accountservice.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Account {

    @Id
    @GeneratedValue
    private Long accountId;
    private String customerName;
    private BigDecimal balance;

    private Account() { 
        super();
    } 
    
    public Long getAccountId()
    {
        return this.accountId;
    }

    public void setAccountId(Long accountId)
    {
        this.accountId = accountId;
    }

    public String getCustomerName()
    {
        return this.customerName;
    }

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public BigDecimal getBalance()
    {
        return this.balance;
    }

    public void setBalance (BigDecimal balance)
    {
        this.balance = balance;
    }

}